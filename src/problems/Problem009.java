package problems;

import util.AndyMath;

/**
 * 
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for
 * which, a2 + b2 = c2
 * <p>
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 * <p>
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find
 * the product abc.
 * 
 * @author atill
 * 
 */
public class Problem009 {
    public static void main(String[] args) {
        Integer result = 0;

        outerloop:
        for (int i = 1; i < 999; i++) {
            for (int j = 1; j < 999; j++) {
                for (int k = 1; k < 999; k++) {
                    if(AndyMath.isPythagoreanTriplet(i, j, k)) {
                        if((i + j + k) == 1000) {
                            result = (i * j * k);
                            break outerloop;
                        }
                    }
                }
            }
        }
        
        System.out.println(result);
    }
}
