package problems;

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1
 * to 10 without any remainder.
 * <p>
 * What is the smallest positive number that is evenly divisible by all of the
 * numbers from 1 to 20?
 * 
 * @author atill
 * 
 */
public class Problem005 {
    private static final int MAX_NUMBER = 20;

    public static void main(String[] args) {
        int i = 0;
        int j;
        
        while(true) {
            ++i;
            for (j = 1; j < MAX_NUMBER; j++) {
                if(i % j != 0) {
                    break;
                }
            }
            
            if(j == MAX_NUMBER) {
                break;
            }
        }
        
        System.out.println(i);
    }
}
