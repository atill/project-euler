package util;

public class AndyMath {
    public static boolean isPrime(final long number) {
        if (number == 2)
            return true;
        if (number < 2)
            return false;
        if(number % 2 == 0)
            return false;
        
        long maxCheck = (long) Math.sqrt(number);

        for (long i = 3; i <= maxCheck; i += 2) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    public static boolean isPythagoreanTriplet(int a, int b, int c) {
        if(! (a < b && b < c)) {
            return false;
        }
        return (((a*a) + (b*b)) == (c*c));
    }
}
