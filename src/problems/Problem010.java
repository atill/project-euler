package problems;

import util.AndyMath;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * <p>
 * Find the sum of all the primes below two million.
 * 
 * @author atill
 * 
 */
public class Problem010 {
    public static void main(String[] args) {
        long result = 0;
        for (int i = 0; i < 2000000; i++) {
            if (AndyMath.isPrime(i)) {
                result += i;
            }
        }
        System.out.println(result);
    }
}
