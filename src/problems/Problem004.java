package problems;

/**
 * 
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 � 99.
 * <p>
 * Find the largest palindrome made from the product of two 3-digit numbers.
 * 
 * @author atill
 * 
 */
public class Problem004 {
    private static final int MAX_NUMBER = 999;

    public static void main(String[] args) {

        Integer result = -1;

        for (int i = 1; i <= MAX_NUMBER; i++) {
            for (int j = 1; j <= MAX_NUMBER; j++) {
                int n = i * j;

                if (n > result && n > 9) {
                    char[] chars = Integer.toString(n).toCharArray();

                    boolean match = true;
                    
                    for (int k = 0; k < (chars.length / 2); k++) {
                        if(chars[k] != chars[(chars.length - (k + 1))]) {
                            match = false;
                        }
                    }
                    
                    if(match) {
                        result = n;
                    }
                }
            }
        }
        
        System.out.println(result);
    }
}
