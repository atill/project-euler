package problems;

import util.AndyMath;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * <p>
 * What is the largest prime factor of the number 600851475143 ?
 * 
 * @author atill
 * 
 */
public class Problem003 {
    
    static final long MAX_NUMBER = 600851475143L;
    
    public static void main(String[] args) {
        long result = 0;
        long i = (long) Math.sqrt(MAX_NUMBER);
        
        while(true) {
            
            --i;
            if(i == 0) {
                break;
            }
            
            if((MAX_NUMBER % i == 0) && AndyMath.isPrime(i)) {
                result = i;
                break;
            }
        }
        
        System.out.println(result);
    }
}
