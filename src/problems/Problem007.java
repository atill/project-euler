package problems;

import util.AndyMath;

/**
 * 
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
 * that the 6th prime is 13.
 * 
 * What is the 10001st prime number?
 * 
 * @author atill
 * 
 */
public class Problem007 {
    private static final int MAX_NUMBER = 10001;

    public static void main(String[] args) {
        long i = 1, primeCounter = 0;
        
        while(true) {
            if(AndyMath.isPrime(i)) {
                primeCounter++;
                
                if(primeCounter == MAX_NUMBER) {
                    break;
                }
            }
            i++;
        }
        
        System.out.println(i);
    }
}
